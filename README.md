# Daemon React - Reaction Role Discord Bot
![Daemon React 0.0.1](https://img.shields.io/badge/Daemon%20React-0.0.1-blue.svg)
![Python 3.5.3+](https://img.shields.io/badge/python-3.5.3+-blue.svg)
![discord.py rewrite](https://img.shields.io/badge/discord.py-1.2.5+-blue.svg)

![Daemon React Example Embed](https://i.imgur.com/rPnxg3P.png)

Light yet powerful reaction role bot coded in discord.py (rewrite branch).

## Features
- Create multiple custom embedded messages with custom reactions and roles
- Automatically assign/remove roles to users when they select/deselect a certain reaction
- Use the same instance of the bot on multiple servers
- Easy installation and setup.
- Error reporting to your own Discord server
- No need to rely on developer mode and IDs.

You can host the bot yourself by configuring the `config.ini` file (manually or via `setup.py`).

## Commands
All commands require an admin role which you can set by using `dr!admin` (requires administrator permissions on the server). The bot will reply with missing permissions otherwise. In the following list the default prefix `dr!` is used, but it can be freely changed in the `config.ini` file.

- `dr!help` shows this set of commands along with a link to the repository.
- `dr!new` starts the creation process for a new reaction role message.
- `dr!abort` aborts the creation process for a new reaction role message started by the command user in that channel.
- `dr!edit` edits an existing reaction role message or provides instructions on how to do so if no arguments are passed.
- `dr!colour` changes the colour of the embeds of new and newly edited reaction role messages.
- `dr!activity` adds an activity for the bot to loop through and show as status.
- `dr!rm-activity` removes an activity from the bot's list.
- `dr!activitylist` lists the current activities used by the bot as statuses.
- `dr!rm-embed` suppresses the embed of an existing reaction-role message or provides instructions on how to do so if no arguments are passed.
- `dr!admin` adds the mentioned role or role id to the admin list, allowing members with a certain role to use the bot commands. Requires administrator permissions on the server.
- `dr!rm-admin` removes the mentioned role or role id from the admin list, preventing members with a certain role from using the bot commands. Requires administrator permissions on the server.
- `dr!adminlist` lists the current admins on the server the command was run in by mentioning them and the current admins from other servers by printing out the role IDs. Requires administrator permissions on the server.
- `dr!kill` shuts down the bot.
- `dr!systemchannel` updates the system channel where the bot sends errors and update notifications.
- `dr!restart` restarts the bot.
- `dr!version` reports the bot's current version.
